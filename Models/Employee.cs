﻿namespace HRAppCRUDOperationsCodeFirst.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public DateTime DOB { get; set; }
        public DateTime HiringDate { get; set; }
        public decimal GrossSalary { get; set; }
        public decimal NetSalary { get; set; }
        public Department Department { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }
}
