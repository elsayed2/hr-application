﻿using Microsoft.EntityFrameworkCore;

namespace HRAppCRUDOperationsCodeFirst.Models
{
    public class ApplicationDbContext :DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Configuration for properties in the Employeee table
            modelBuilder.Entity<Employee>(e=> 
            {
                // Make the Name column required and its max length is 100
                e.Property(e => e.Name).IsRequired();
                e.Property(e => e.Name).HasMaxLength(100);
                e.Property(e => e.Name).HasColumnType("varchar(100)");

                // Make the City column required and its max length is 50
                e.Property(e => e.City).IsRequired();
                e.Property(e => e.City).HasMaxLength(50);
                e.Property(e => e.City).HasColumnType("varchar(50)");

                // Make the data type for the Date of Birth column date   
                e.Property(e => e.DOB).HasColumnName("Date of Birth");
                e.Property(e => e.DOB).HasColumnType("date");

                // Make the data type for the Hiring Date column date
                e.Property(e => e.HiringDate).HasColumnName("Hiring Date");
                e.Property(e => e.HiringDate).HasColumnType("date");

                // Make the data type for the Gross Salary column decimal(12,2)
                e.Property(e => e.GrossSalary).HasColumnType("decimal(12,2)");
                e.Property(e => e.GrossSalary).HasColumnName("Gross Salary");

                // Make the data type for the Net Salary column decimal(12,2)
                e.Property(e => e.NetSalary).HasColumnType("decimal(12,2)");
                e.Property(e => e.NetSalary).HasColumnName("Net Salary");

                // Don't make a column for DepartmentName property and its diplay name is Department 
                e.Property(e => e.DepartmentName).HasAnnotation("DisplayName", "Department");
                e.Ignore(e => e.DepartmentName);

            });

            // Configuration for properties in the Departments table
            modelBuilder.Entity<Department>(d =>
            {
                // Make the Name column required and its type is varchar(150)
                d.Property(dep => dep.Name).IsRequired();  
                d.Property(dep => dep.Name).HasColumnType("varchar(150)");

                // Make the data type for the Department Abbreviation column varchar(5) 
                d.Property(dep => dep.Abbr).HasColumnName("Department Abbreviation");
                d.Property(dep => dep.Abbr).HasColumnType("varchar(5)");
            });

            //Relation between Departments and Employees Table
            modelBuilder.Entity<Department>()
                .HasMany(d => d.Employees)
                .WithOne(e => e.Department)
                .HasForeignKey(e => e.DepartmentId);


        }
    }
}
