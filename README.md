# HR Application 


## Description 
This is a website for HR management for CRUD operations code first. You can add new users or modify user data. You can search for a user. You can sort the users in ascending or descending order based on any label (Id, Name, Salary, Department name, etc.) by clicking on the label. ![HRApp](/HRApp.jpg)
