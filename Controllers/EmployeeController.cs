﻿using HRAppCRUDOperationsCodeFirst.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HRAppCRUDOperationsCodeFirst.Controllers
{
    public enum SortDirection
    {
        Ascending,  
        Descending
    }

    public class EmployeeController : Controller
    {
        private readonly ApplicationDbContext _context;
        public EmployeeController(ApplicationDbContext context)
        {
            _context = context;
        }

        // Get all employees and their departments
        private List<Employee> GetEmployees()
        {
             var employees = (from d in _context.Departments
                              join e in _context.Employees
                              on d.Id equals e.DepartmentId
                              select new Employee
                              {
                                  Id = e.Id,
                                  Name = e.Name,   
                                  City=e.City,
                                  DOB=e.DOB,
                                  HiringDate=e.HiringDate,
                                  GrossSalary=e.GrossSalary,
                                  NetSalary=e.NetSalary,
                                  DepartmentName=d.Name

                              }).ToList();
            return employees;
        }

        // Sort all Employees according to a specific field
        private List<Employee> SortEmployees(List<Employee> employees,string sortField,string currentSortField, SortDirection sortDirection)
        {
            if(string.IsNullOrEmpty(sortField))
            {
                ViewBag.SortField = "Id";
                ViewBag.SortDirection = SortDirection.Ascending;
            }
            else
            {
                if (currentSortField==sortField)
                    ViewBag.SortDirection = sortDirection == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;
                else
                    ViewBag.SortDirection = SortDirection.Ascending;
                ViewBag.SortField = sortField;
            }
            var propertyInfo = typeof(Employee).GetProperty(ViewBag.SortField);
            if (ViewBag.SortDirection == SortDirection.Ascending)
                employees = employees.OrderBy(e => propertyInfo.GetValue(e, null)).ToList();
            else
                employees=employees.OrderByDescending(e=> propertyInfo.GetValue(e, null)).ToList();

            return employees;
        }

        // Display all Employees
        public IActionResult Index(string sortField,string currentSortField, SortDirection sortDirection, string searchByName)
        {
            var employees = GetEmployees();
            if (!string.IsNullOrEmpty(searchByName))
                employees = employees.Where(e => e.Name.ToLower().Contains(searchByName.ToLower())).ToList();
            return View(this.SortEmployees(employees,sortField,currentSortField, sortDirection));
        }

        // Create a new employee
        public IActionResult Create()
        {
            ViewBag.departments = _context.Departments.ToList();
            return View();
        }

        [HttpPost]
        public IActionResult Create(Employee emp)
        {
            ViewBag.Departments = _context.Departments.ToList();

            ModelState.Remove("Department");
            ModelState.Remove("DepartmentName");
            if (ModelState.IsValid)
            {
                _context.Employees.Add(emp);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("Create", emp);
        }

        // Diplay employee details
        public IActionResult Details(int id)
        {
            var employee = _context.Employees.Include(e => e.Department).FirstOrDefault(e => e.Id == id);
            return View(employee);
        }
        
        // Edit an employee
        public IActionResult Edit(int id)
        {
            var employee=_context.Employees.Where(e=> e.Id==id).SingleOrDefault();
            ViewBag.Departments = _context.Departments.ToList();
            return View("Create", employee);
        }

        [HttpPost]
        public IActionResult Edit(Employee emp)
        {

            ModelState.Remove("Department");
            ModelState.Remove("DepartmentName");
            if (ModelState.IsValid)
            {
                _context.Employees.Update(emp);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Departments = _context.Departments.ToList();
            return View("Create", emp);
        }

        // Dlete an employee
        public IActionResult Delete(int id)
        {
            var employee=_context.Employees.FirstOrDefault(e=> e.Id == id);    
            if(employee!=null)
            {
                _context.Employees.Remove(employee);
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

    }
}
